 IF NOT EXISTS(SELECT * FROM sys.databases WHERE name = 'Comdata')
  BEGIN
    CREATE DATABASE [Comdata]

    END
    GO
       USE [Comdata]
    GO
--Verificamos que no exista
IF NOT EXISTS (SELECT * FROM sysobjects WHERE name='Colaborador' and xtype='U')
BEGIN
    CREATE TABLE Colaborador (
		IdColaborador int PRIMARY KEY IDENTITY(1,1),
		NumeroIdentificacion nvarchar(20) NOT NULL,
		Nombres nvarchar(40),
		Apellidos nvarchar(40),
		Direccion nvarchar(20),
		Email nvarchar(40),
		Telefono nvarchar(20),
		Salario int,
		AreaPertenece nvarchar(40),
		FechaIngreso date,
		Sexo nvarchar(20),
)
END