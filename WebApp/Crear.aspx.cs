﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Text;
using System.Web.Script.Serialization;
using System.Web.UI;

namespace WebApplication2
{
    public partial class Crear : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void BtnCrear_Click(object sender, EventArgs e)
        {
            try
            {
                string numDocumento = TxtNumeroDocumentoB.Text.ToString();
                string nombres = TxtNombresB.Text.ToString();
                string apellidos = TxtApellidosB.Text.ToString();
                string direccion = TxtDireccionB.Text.ToString();
                string email = TxtEmailB.Text.ToString();
                string telefono = TxtTelefonoB.Text.ToString();
                string salario = TxtSalarioB.Text.ToString();
                string areaPertenece = TxtAreaPerteneceB.Text.ToString();
                string fechaIngreso = TxtFechaIngresoB.Text.ToString();
                string sexo = TxtSexoB.Text.ToString();

                HttpClient clienteHttp = new HttpClient();
                clienteHttp.BaseAddress = new Uri(ConfigurationManager.AppSettings["endpoint"].ToString());

                var request = clienteHttp.PostAsync("Colaboradores?id=" + numDocumento, "", new JsonMediaTypeFormatter()).Result;
                //LimpiarFormulario();

                if (request.IsSuccessStatusCode)
                {
                    var resultString = request.Content.ReadAsStringAsync().Result;
                    var correcto = JsonConvert.DeserializeObject<Dictionary<string, string>>(resultString);

                    if (correcto.Count > 0)
                    {
                        alert("Numero Documento, ya existe");
                    }
                }
                else
                {
                    HttpClient clienteHttp2 = new HttpClient();
                    clienteHttp2.BaseAddress = new Uri(ConfigurationManager.AppSettings["endpoint"].ToString());
                    clienteHttp2.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    string json = new JavaScriptSerializer().Serialize(new
                    {
                        NumeroIdentificacion = numDocumento,
                        Nombres = nombres,
                        Apellidos = apellidos,
                        Direccion = direccion,
                        Email = email,
                        Telefono = telefono,
                        Salario = salario,
                        AreaPertenece = areaPertenece,
                        Sexo = sexo
                    });


                    StringContent content = new StringContent(json, Encoding.UTF8, "application/json");
                    var request2 = clienteHttp2.PostAsync("Colaboradores", content).Result;


                    if (request2.IsSuccessStatusCode)
                    {
                        var resultString = request2.Content.ReadAsStringAsync().Result;
                        var correcto = JsonConvert.DeserializeObject<Dictionary<string, string>>(resultString);

                        if (correcto.Count > 0)
                        {
                            LimpiarFormulario();
                            alert("Creado");
                        }
                    }
                    else
                    {
                        alert("algo fallo.");
                    }
                }
            }
            catch (Exception ex)
            {
                alert(ex.Message);
            }
        }

        protected void BtnLimpiar_Click(object sender, EventArgs e)
        {
            LimpiarFormulario();
        }

        public void LimpiarFormulario()
        {
            TxtNumeroDocumentoB.Text = "";
            TxtNombresB.Text = "";
            TxtApellidosB.Text = "";
            TxtDireccionB.Text = "";
            TxtEmailB.Text = "";
            TxtTelefonoB.Text = "";
            TxtSalarioB.Text = "";
            TxtAreaPerteneceB.Text = "";
            TxtFechaIngresoB.Text = "";
            TxtSexoB.Text = "";

            //DivResultado.Visible = false;
        }

        private void alert(string message)
        {
            Response.Write("<script>alert('" + message + "')</script>");
        }
    }
}