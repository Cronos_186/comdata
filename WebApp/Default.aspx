﻿<%@ Page Title="Inicio" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="WebApplication2._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h1>Buscar Colaborador</h1>

    <div class="jumbotron">
        <p class="lead">
            <asp:Label runat="server">Numero Documento</asp:Label>
            <asp:TextBox ID="TxtNumeroDocumento" runat="server" required="required" />
        </p>
        <p>
            <asp:Button ID="BtnBuscar" CssClass="btn btn-primary btn-lg" runat="server" Text="Buscar" OnClick="BtnBuscar_Click" />
        </p>
    </div>

    <div class="jumbotron" id="DivResultado" runat="server">
        <h1>Resultado Busqueda</h1>
        <p class="lead">
            <asp:Label runat="server">Numero Documento</asp:Label>
            <asp:TextBox ID="TxtNumeroDocumentoB" runat="server" Enabled="False" ReadOnly="true" />
        </p>
        <p>
        <p class="lead">
            <asp:Label runat="server">Nombres</asp:Label>
            <asp:TextBox ID="TxtNombresB" runat="server" Enabled="False" ReadOnly="true" />
        </p>
        <p class="lead">
            <asp:Label runat="server">Apellidos</asp:Label>
            <asp:TextBox ID="TxtApellidosB" runat="server" Enabled="False" ReadOnly="true" />
        </p>
        <p class="lead">
            <asp:Label runat="server">Dirección</asp:Label>
            <asp:TextBox ID="TxtDireccionB" runat="server" Enabled="False" ReadOnly="true" />
        </p>
        <p class="lead">
            <asp:Label runat="server">Email</asp:Label>
            <asp:TextBox ID="TxtEmailB" runat="server" Enabled="False" ReadOnly="true" />
        </p>
        <p class="lead">
            <asp:Label runat="server">Telefono</asp:Label>
            <asp:TextBox ID="TxtTelefonoB" runat="server" Enabled="False" ReadOnly="true" />
        </p>
        <p class="lead">
            <asp:Label runat="server">Salario</asp:Label>
            <asp:TextBox ID="TxtSalarioB" runat="server" Enabled="False" ReadOnly="true" />
        </p>
        <p class="lead">
            <asp:Label runat="server">AreaPertenece</asp:Label>
            <asp:TextBox ID="TxtAreaPerteneceB" runat="server" Enabled="False" ReadOnly="true" />
        </p>
        <p class="lead">
            <asp:Label runat="server">Fecha Ingreso</asp:Label>
            <asp:TextBox ID="TxtFechaIngresoB" runat="server" Enabled="False" ReadOnly="true" />
        </p>
        <p class="lead">
            <asp:Label runat="server">Sexo</asp:Label>
            <asp:TextBox ID="TxtSexoB" runat="server" Enabled="False" ReadOnly="true" />
        </p>
        <p>
            <asp:Button ID="BtnLimpiar" CssClass="btn btn-primary btn-lg" runat="server" Text="Limpiar" OnClick="BtnLimpiar_Click" />
        </p>
    </div>
</asp:Content>
