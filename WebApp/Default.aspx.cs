﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Web.UI;

namespace WebApplication2
{
    public partial class _Default : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            LimpiarFormulario();
        }

        public void LimpiarFormulario()
        {
            TxtNumeroDocumentoB.Text = "";
            TxtNombresB.Text = "";
            TxtApellidosB.Text = "";
            TxtDireccionB.Text = "";
            TxtEmailB.Text = "";
            TxtTelefonoB.Text = "";
            TxtSalarioB.Text = "";
            TxtAreaPerteneceB.Text = "";
            TxtFechaIngresoB.Text = "";
            TxtSexoB.Text = "";

            DivResultado.Visible = false;
        }

        protected void BtnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                string numDocumento = TxtNumeroDocumento.Text.ToString();

                HttpClient clienteHttp = new HttpClient();
                clienteHttp.BaseAddress = new Uri(ConfigurationManager.AppSettings["endpoint"].ToString());

                var request = clienteHttp.PostAsync("Colaboradores?id=" + numDocumento, numDocumento, new JsonMediaTypeFormatter()).Result;
                LimpiarFormulario();

                if (request.IsSuccessStatusCode)
                {
                    var resultString = request.Content.ReadAsStringAsync().Result;
                    var correcto = JsonConvert.DeserializeObject<Dictionary<string, string>>(resultString);


                    if (correcto.Count > 0)
                    {
                        TxtNumeroDocumentoB.Text = correcto["NumeroIdentificacion"];
                        TxtNombresB.Text = correcto["Nombres"];
                        TxtApellidosB.Text = correcto["Apellidos"];
                        TxtDireccionB.Text = correcto["Direccion"];
                        TxtEmailB.Text = correcto["Email"];
                        TxtTelefonoB.Text = correcto["Telefono"];
                        TxtSalarioB.Text = correcto["Salario"];
                        TxtAreaPerteneceB.Text = correcto["AreaPertenece"];
                        TxtFechaIngresoB.Text = correcto["FechaIngreso"];
                        TxtSexoB.Text = correcto["Sexo"];

                        DivResultado.Visible = true;
                    }
                }
                else
                {
                    alert("No hay registros");
                }
            }
            catch (Exception ex)
            {
                alert(ex.Message);
            }
        }

        protected void BtnLimpiar_Click(object sender, EventArgs e)
        {
            LimpiarFormulario();
        }

        private void alert(string message)
        {
            Response.Write("<script>alert('" + message + "')</script>");
        }
    }
}