﻿<%@ Page Title="Eliminar" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Eliminar.aspx.cs" Inherits="WebApplication2.Eliminar" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h1>Eliminar Colaborador</h1>
    
    <div class="jumbotron">
        <p class="lead">
            <asp:Label runat="server">ID</asp:Label>
            <asp:TextBox ID="TxtID" runat="server" required="required" />
        </p>
        <p>
            <asp:Button ID="BtnEliminar" CssClass="btn btn-primary btn-lg" runat="server" Text="Eliminar" OnClick="BtnEliminar_Click"/>

        </p>
        <p>
            <asp:Button ID="BtnLimpiar" CssClass="btn btn-primary btn-lg" runat="server" Text="Limpiar" OnClick="BtnLimpiar_Click" />
        </p>
    </div>
</asp:Content>
