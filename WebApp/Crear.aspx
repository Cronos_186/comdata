﻿<%@ Page Title="Crear" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Crear.aspx.cs" Inherits="WebApplication2.Crear" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h1>Crear Colaborador</h1>

    <div class="jumbotron" id="DivResultado" runat="server">
        <p class="lead">
            <asp:Label runat="server">Numero Documento</asp:Label>
            <asp:TextBox ID="TxtNumeroDocumentoB" runat="server" required="required" />
        </p>
        <p>
        <p class="lead">
            <asp:Label runat="server">Nombres</asp:Label>
            <asp:TextBox ID="TxtNombresB" runat="server" required="required" />
        </p>
        <p class="lead">
            <asp:Label runat="server">Apellidos</asp:Label>
            <asp:TextBox ID="TxtApellidosB" runat="server" />
        </p>
        <p class="lead">
            <asp:Label runat="server">Dirección</asp:Label>
            <asp:TextBox ID="TxtDireccionB" runat="server" />
        </p>
        <p class="lead">
            <asp:Label runat="server">Email</asp:Label>
            <asp:TextBox ID="TxtEmailB" runat="server" />
        </p>
        <p class="lead">
            <asp:Label runat="server">Telefono</asp:Label>
            <asp:TextBox ID="TxtTelefonoB" runat="server" />
        </p>
        <p class="lead">
            <asp:Label runat="server">Salario</asp:Label>
            <asp:TextBox ID="TxtSalarioB" runat="server" />
        </p>
        <p class="lead">
            <asp:Label runat="server">Area Pertenece</asp:Label>
            <asp:TextBox ID="TxtAreaPerteneceB" runat="server" />
        </p>
        <p class="lead">
            <asp:Label runat="server" Visible="false">Fecha Ingreso</asp:Label>
            <asp:TextBox ID="TxtFechaIngresoB" runat="server" Visible="false" />
        </p>
        <p class="lead">
            <asp:Label runat="server">Sexo</asp:Label>
            <asp:TextBox ID="TxtSexoB" runat="server" />
        </p>
        <p>
            <asp:Button ID="BtnBuscar" CssClass="btn btn-primary btn-lg" runat="server" Text="Crear" OnClick="BtnCrear_Click" />
        </p>
        <p>
            <asp:Button ID="BtnLimpiar" CssClass="btn btn-primary btn-lg" runat="server" Text="Limpiar" OnClick="BtnLimpiar_Click" />
        </p>
    </div>
</asp:Content>
