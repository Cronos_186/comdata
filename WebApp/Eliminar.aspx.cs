﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using Newtonsoft.Json;
using System.Configuration;
using System.Net.Http;
using System.Net.Http.Formatting;

namespace WebApplication2
{
    public partial class Eliminar : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void BtnEliminar_Click(object sender, EventArgs e)
        {
            try
            {
                string id = TxtID.Text.ToString();

                HttpClient clienteHttp = new HttpClient();
                clienteHttp.BaseAddress = new Uri(ConfigurationManager.AppSettings["endpoint"].ToString());

                var request = clienteHttp.DeleteAsync("Colaboradores/" + id).Result;
                //LimpiarFormulario();

                if (request.IsSuccessStatusCode)
                {
                    var resultString = request.Content.ReadAsStringAsync().Result;
                    var correcto = JsonConvert.DeserializeObject<Dictionary<string, string>>(resultString);


                    if (correcto.Count > 0)
                    {
                        LimpiarFormulario();
                        alert("Elimiado");
                    }
                }
                else
                {
                    alert("No hay registros");
                }
            }
            catch (Exception ex)
            {
                alert(ex.Message);
            }
        }

        public void LimpiarFormulario()
        {
            TxtID.Text = "";
        }

        protected void BtnLimpiar_Click(object sender, EventArgs e)
        {
            LimpiarFormulario();
        }

        private void alert(string message)
        {
            Response.Write("<script>alert('" + message + "')</script>");
        }
    }
}