﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public class ColaboradoresController : ApiController
    {
        private Model1 db = new Model1();

        // GET: api/Colaboradores
        public IQueryable<Colaborador> GetColaborador()
        {
            return db.Colaborador;
        }

        // GET: api/Colaboradores/5
        [ResponseType(typeof(Colaborador))] //read
        public IHttpActionResult ConsultarColaboradorPorIdentificacion(string id)
        {
            Colaborador colaborador = db.Colaborador.Where(r => r.NumeroIdentificacion == id).FirstOrDefault();
            if (colaborador == null)
            {
                return NotFound();
            }

            return Ok(colaborador);
        }

        // PUT: api/Colaboradores/5
        [ResponseType(typeof(void))] //update
        public IHttpActionResult PutColaborador(int id, Colaborador colaborador)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != colaborador.IdColaborador)
            {
                return BadRequest();
            }

            db.Entry(colaborador).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ColaboradorExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Colaboradores
        [ResponseType(typeof(Colaborador))] //create
        public IHttpActionResult RegistrarColaborador(Colaborador colaborador)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Colaborador.Add(colaborador);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = colaborador.IdColaborador }, colaborador);
        }

        // DELETE: api/Colaboradores/5
        [ResponseType(typeof(Colaborador))] //delete
        public IHttpActionResult DeleteColaborador(int id)
        {
            Colaborador colaborador = db.Colaborador.Find(id);
            if (colaborador == null)
            {
                return NotFound(); //no existe
            }

            db.Colaborador.Remove(colaborador);
            db.SaveChanges();

            return Ok(colaborador);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ColaboradorExists(int id)
        {
            return db.Colaborador.Count(e => e.IdColaborador == id) > 0;
        }
    }
}