namespace WebApplication1.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Colaborador")]
    public partial class Colaborador
    {
        [Key]
        public int IdColaborador { get; set; }

        [Required]
        [StringLength(20)]
        public string NumeroIdentificacion { get; set; }

        [StringLength(40)]
        public string Nombres { get; set; }

        [StringLength(40)]
        public string Apellidos { get; set; }

        [StringLength(20)]
        public string Direccion { get; set; }

        [StringLength(40)]
        public string Email { get; set; }

        [StringLength(20)]
        public string Telefono { get; set; }

        public int? Salario { get; set; }

        [StringLength(40)]
        public string AreaPertenece { get; set; }

        [Column(TypeName = "date")]
        public DateTime? FechaIngreso { get; set; }

        [StringLength(20)]
        public string Sexo { get; set; }
    }
}
